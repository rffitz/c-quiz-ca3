#include <string>
using namespace std;

class Score
{
public:
	Score()
	{

	}
	Score(string quiz,string user,int score)
	{
		this->quiz = quiz;
		this->user = user;
		this->score = score;
	}
	int get_score()
	{
		return this->score;
	}
	string get_quiz()
	{
		return this->quiz;
	}
	string get_user()
	{
		return this->user;
	}

	bool operator == (Score sc) 
	{
		return  sc.get_score() == this->score;
	}

	bool operator != (Score sc)
	{
		return  sc.get_score() != this->score;
	}
	bool operator <= (Score sc)
	{
		return  sc.get_score() >= this->score;
	}
	bool operator >= (Score sc)
	{
		return  sc.get_score() <= this->score;
	}
	bool operator > ( Score sc)
	{
		return  sc.get_score() < this->score;
	}
	bool operator < (Score sc)
	{
		return  sc.get_score() > this->score;
	}


private:
	string quiz;
	string user;
	int score;
};