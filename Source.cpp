#include <iostream>
#include <time.h>
#include <fstream>
#include <string>
#include "ResizeableArray.h"
#include "Quiz.h"
#include <regex>
#include "OrderedArray.h"
#include "Score.h"

using namespace std;



void show_high_scores()
{
	string fileName = "scores.txt";
	string line;
	string token;
	int score;
	string quiz;
	string name;

	
	int pos =0;
	string::size_type sz;
	ifstream myfile(fileName);
	//I chose an ordered array for the reading in of scores as it will take in scores and order them
	//efficiently
	OrderedArray<Score> score_list(10);
	if (myfile.is_open())
	{
		
		while (getline(myfile, line))
		{
			istringstream ss(line);
			while (getline(ss, token, ','))
			{
				if (pos == 0)
				{
					quiz = token;
				}
				else if (pos == 1)
				{
					name = token;
				}
				else
				{
					score = std::stoi(token, &sz);
				}

				pos++;
			}
			score_list.push(Score(quiz, name, score));
			pos = 0;
		}
		
	}
	for (int i = score_list.length()-1; i >= 0; i--)
	{
		cout << "Quiz:   " << score_list.getElement(i).get_quiz() << endl;
		cout << "User:   "<<score_list.getElement(i).get_user() << endl;
		cout << "Score:  "<<score_list.getElement(i).get_score() << endl;

		cout << endl ;
	}
	system("PAUSE");

}
inline void display_score(string name, int score)
{

	cout << "============================" << endl;
	cout << "YOU COMPLETED THE QUIZ WITH "  << score << " CORRECT ANSWERS" <<endl; 

}

inline void save_score(string quiz,string name,int score)
{
	ofstream outfile;
	quiz.erase(remove(quiz.begin(), quiz.end(), ' '), quiz.end());
	outfile.open("scores.txt", std::ios_base::app);
	outfile <<quiz<<","<<name<<","<<score<<"\n";

}
int validate_Number(int high,int low)
{
	
	string number;
	int valid;
	bool is_valid = false;
	while (!is_valid)
	{
		cin >> number;
		if (regex_match(number,regex("[1-9]{1,}")))
		{			
			string::size_type sz;  
		    valid = std::stoi(number, &sz);
			if (valid < high && valid > low)
			{
				is_valid = true;
			}
			else
			{
				cout << "Not within correct Range"<<endl;
			}
		}
		else
		{
			cout << "Not  a number" << endl;
		}
	}	
	return valid;
}
inline bool iequals(string a, string b)
{
	int length = 0;
	if (a.size() > b.size())
	{
		length = a.size();
	}
	else
	{
		length = b.size();
	}
	for (unsigned int i = 0; i < length; ++i)
	{
		if (tolower(a[i]) != tolower(b[i]))
			return false;
	}
	return true;
}
ResizeableArray<Quiz*> read()
{
	//chose the double linked list as it will allow fast removals
	//and also lets me randomise by putting in left or right

	string fileName = "quiz.csv";
	string line;
	//I chose a resizeablearray here to allow many questions to be entered here efficiently
	//it also allows quick traversal which is what is needed as there is no searching needed
	//it my program
	ResizeableArray<Question>* questions;
	questions = new ResizeableArray<Question>(10);
	int pos = 0;
	ResizeableArray<Quiz*> quizs;
	quizs = ResizeableArray<Quiz*>(5);
	string temp = "";
	ifstream myfile(fileName);
	Quiz* qz;
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{

			Question q1 = Question(line);

			if (iequals(q1.getType(),"quiz"))
			{

				if (pos != 0)
				{
					quizs.push(new Quiz(temp, questions));

					questions->clear();
				}
				temp = q1.getQuestion();
				pos++;

			}
			questions->push(Question(line));



		}

		quizs.push(new Quiz(temp, questions));


		myfile.close();
		return quizs;
	}
}



inline string start()
{
	cout << "============ QUIZ =============" << endl;
	string name;
	cout << "===============================" << endl;
	cout << "Please enter name of player?" << endl;
	cout << "===============================" << endl;
	cin >> name;
	return name;


}

inline int quizMenu(ResizeableArray<Quiz*> quizs)
{
	cout << "===============================" << endl;
	cout << "Please choose an option between 1 and " << (quizs.length()+2) << endl;
	cout << "===============================" << endl;
	for (int i = 0; i < quizs.length(); i++)
	{
		cout << (i + 1) << ":)" << quizs.getElement(i)->getTitle() << endl;
	}
	cout << (quizs.length() + 1) << ":) View all scores" << endl;
	cout << (quizs.length() + 2) << ":) Exit" << endl;
	cout << "===============================" << endl;

	
	return validate_Number(quizs.length()+3,0)-1;
}
int startQuiz(ResizeableArray<Question>* questions)
{
	cin.clear();
	cin.ignore();
	int answered = 0;
	int score = 0;
	int choice;
	string ans;
	srand(time(NULL));
	int  next;
	while (answered <= questions->length() +1)
	{
		next = rand() % questions->length();
		if (iequals(questions->getElement(next).getType(),"Quiz"))
		{
			questions->getElement(next).setUsed(true);
			answered++;
		}
		if (!questions->getElement(next).getUsed())
		{
			
			if (tolower(questions->getElement(next).getType()[0]) == 'm')
			{
				istringstream ss(questions->getElement(next).getQuestion());
				string token;
				ResizeableArray<string> ar = ResizeableArray<string>(5);
				int count = 0;
				while (getline(ss, token, ','))
				{
					ar.push(token);
				}
				cout << ar.getElement(0) << "?" << endl;
				cout << "1)" << ar.getElement(1) << endl;
				cout << "2)" << ar.getElement(2) << endl;
				cout << "3)" << ar.getElement(3) << endl;
				cout << "4)" << ar.getElement(4) << endl;
				choice = validate_Number(ar.length(),0);
				string chosen = ar.getElement(choice);
				string answe = questions->getElement(next).getAnswer();
				chosen.erase(remove(chosen.begin(), chosen.end(), '\n'), chosen.end());
				chosen.erase(remove(chosen.begin(), chosen.end(), ' '), chosen.end());
				answe.erase(remove(answe.begin(), answe.end(), ' '), answe.end());
				if (iequals(chosen, answe))
				{
					
					cout << "Correct answer" << endl;
					score++;
				}
				else
				{
					cout << "WRONG" << endl;
				}

			}
			else
			{
				cout << questions->getElement(next).getQuestion();
				getline(cin, ans);
				
				if (iequals(questions->getElement(next).getAnswer(),ans))
				{
					cout << "Correct answer press enter to continue" << endl;
					score++;
				}
				else
				{
					cout << "WRONG,press enter to continue" << endl;
				}
				
			}
			cin.clear();
			cin.ignore(10000, '\n');
			
			answered++;
			questions->getElement(next).setUsed(true);
			

		}
		
		
	}
	return score;
	cout << score;

	}
	int main()
	{

		ResizeableArray<Quiz*> quizs;
		quizs = ResizeableArray<Quiz*>(5);

		quizs = read();
		string name;
		name = start();
		
		int choice;
		choice = 0;
		choice = quizMenu(quizs);
		int score;
		while (choice != quizs.length()+1)
		{
			if (choice == quizs.length())
			{
				show_high_scores();
			}
			else
			{
				quizs.getElement(choice)->reset();
				score = startQuiz(quizs.getElement(choice)->getQs());
				display_score(name, score);
				save_score(quizs.getElement(choice)->getTitle(), name, score);
				
			}
			choice = quizMenu(quizs);
		}
		



		//HOW TO GET EACH ANSWER FOR MULTIPLE
		/*string qs = q.getquestion();
		istringstream ss(qs);
		string token;
		resizeablearray<string> ar =  resizeablearray<string>(5);
		int count = 0;
		while (getline(ss, token, ','))
		{
		ar.push(token);
		}
		for (int i = 1; i < ar.length(); i++)
		{
		cout <<endl << i << ")" << ar[i];
		}
		cout << endl;*/

		//cout << q.getAnswer().substr(q.getAnswer().find("/n"));
		system("PAUSE");
	}

