#ifndef ORDEREDARRAY_H
#define ORDEREDARRAY_H
#include <malloc.h>
#include <math.h>
#include <iostream>
using namespace std;

template<class T>
class OrderedArray
{
    public:
		//default contructor that initalizes the member variables 
        OrderedArray()
        {
			currentSize = 0;
			grow = 10;
			maxSize = grow;
            theArray = (T*)calloc( grow, sizeof(T));
        }
		//contrcutor that takes in an int that defines how much the array should grow by when it reaches its limit
        OrderedArray(int grow_size)
        {
			currentSize = 0;
            grow = grow_size;
			maxSize = grow;
            theArray = (T*)calloc( grow, sizeof(T));
            maxSize += grow;
        }
		//push method that adds and sorts an element into the array
        void push(const T& newElement)
        {


            if (currentSize+1 > maxSize)
            {
                increaseSize();
            }
            int index = 0;
            int pos = 0;

            T* temp = new T[maxSize];
            bool added = false;
			//the order is established here it iterates through the array finiding 
			//the position where an element is greater than it and it inserts itself before and contiues
			//iterating through the list it has a complexity of (n)
            while(pos <= currentSize)
            {
                if(!added && (theArray[index] > newElement|| pos ==currentSize))
                {
                    temp[pos] = newElement;
                    added = true;
                }
                else
                {
                    temp[pos] = theArray[index];
                    index++;
                }
                 pos++;
            }
            currentSize++;
         
			
            theArray = temp;
        }

        int length()
        {
            return currentSize;
        }

        T getElement(int index)
        {
            if(index < currentSize && index >= 0)
            {
               return theArray[index];
            }


        }

        bool remove(int index)
        {
            if(index > 0 && index < currentSize)
            {
                for(int i = index; i < currentSize; i ++)
                {
                    theArray[i] = theArray[i+1];
                }
                currentSize--;
                return true;
            }
            return false;
        }

        int search(const T& target)
        {
			//The search used is a binary search which has a complexity of
			// O(log(n)) 
            int high = currentSize;
            int low = 0;
            int index = 0;
            bool found = true;
            while (found)
            {
                cout << index << ":" << high << ":" << low << endl;

                index = low+(ceil(high-low)/2);
                if(theArray[index] == target)
                {
                    found = false;
                }
                else if(low == high)
                {

                    found = false;
                }
                else if(theArray[index] < target)
                {
                    low = index+1;
                }
                else if(theArray[index] > target)
                {
                    high = index-1;
                }
            }
            return index;
        }

        void clear()
        {
            maxSize = grow;
            currentSize = 0;
		
            theArray = (T*)calloc( grow, sizeof(T));
        }

        void increaseSize()
        {
            T* temp = new T[maxSize];
            for(int i = 0; i < currentSize-1;i++)
            {
                temp[i] = theArray[i];
            }
			
			
            theArray = temp;
            maxSize = grow +currentSize;
        }




        ~OrderedArray()
		{
			
			delete[] theArray;
		}
    protected:
    private:
		int maxSize;
		T* theArray;
		int currentSize;
		int grow;



};

#endif // ORDEREDARRAY_H

